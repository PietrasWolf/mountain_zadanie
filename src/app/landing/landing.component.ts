import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { markDirty } from '@angular/core/src/render3';

import { Observable } from 'rxjs';
import { EventManagerPlugin } from '@angular/platform-browser/src/dom/events/event_manager';



@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})



export class LandingComponent implements OnInit {
  constructor(private httpClient: HttpClient) {



   }
   public email: string;
   public psw: string;
   public feedback: string;
   public status: string;
  // tslint:disable-next-line:member-ordering
  showMobileMenu = false;
  showPopUp = false;
  @HostListener('document:keydown.escape', ['$event'])
  handle(event: KeyboardEvent) {
    this.showPopUp = false;
  }
  showMenufun() {
    if (this.showMobileMenu === true) {
      this.showMobileMenu = false;
    } else {
      this.showMobileMenu = true;
    }

  }
  hideMenuFun() {
    this.showMobileMenu = false;
  }
  showPopFun() {
    this.showPopUp = true;
    this.email = '';
    this.psw = '';
    this.feedback = '';
  }
  hidePopFun() {
    this.showPopUp = false;

  }

  ngOnInit() {
  }



  logPost() {


    const p = ({
      'login': this.email,
      'password': this.psw

    });

    this.httpClient.post('https://recruitment-api.pyt1.stg.jmr.pl/login', p).subscribe(
      poste => { this.feedback = poste['message']; console.log(poste); });


  }

}
